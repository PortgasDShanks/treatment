<div class="progressbar-heading grids-heading">
	<h2>Change Password</h2>
</div>

<div class="blank">
    <div class="blank-page">
       <div class="panel panel-widget forms-panel">
			<div class="forms">
				<div class=" form-grids form-grids-right">
					<div class="widget-shadow " data-example-id="basic-forms"> 
						<div class="form-body">
							<form class="form-horizontal" id="form_add"> 
								<div class="form-group"> 
									<label for="inputEmail3" class="col-sm-2 control-label">Current Password</label> 
									<div class="col-sm-9"> 
										<input type="password" name="cur_pw" required class="form-control" id="current_pw" placeholder="Current Password"> 
									</div> 
								</div> 
								<div class="form-group"> 
									<label for="inputPassword3" class="col-sm-2 control-label">New Password</label> 
									<div class="col-sm-9"> 
										<input type="password" name="new_pw" required class="form-control" id="new_pw" placeholder="New Password"> 
									</div> 
								</div>  
								<div class="form-group"> 
									<label for="inputPassword3" class="col-sm-2 control-label">Re-enter Password</label> 
									<div class="col-sm-9"> 
										<input type="password" name="re_pw" required class="form-control" id="re_pw" placeholder="Re-enter Password"> 
									</div> 
								</div> 
								<div class="col-sm-offset-2"> 
									<button type="submit" class="btn btn-default w3ls-button">Update</button> 
								</div> 
							</form> 

						</div>
					</div>
				</div>
			</div>	
		</div>
    </div>
</div>

<script src="js/jquery2.0.3.min.js"></script>

<script>
$(document).ready(function(){

  $("#form_add").submit(function(e){
  e.preventDefault();

  var new_pw = $("#new_pw").val();
  var re_pw = $("#re_pw").val();

  if(new_pw == re_pw){
  	 $.ajax({
	    url:"ajax/change_pw.php",
	    method:"POST",
	    data:$(this).serialize(),
	    success:function(data){

	    if(data==1){
	    	notify('white','Successfully Added');
	    }else if(data==2){
	      notify('black','Current Password not found in databases.');
	    }else{
	       notify('error','Duplicate Entry');
	    }
	    }
	  })
  }else{
  	notify('black','New password and Re-enter password doesnt match');
  }
 
   });
});
</script>