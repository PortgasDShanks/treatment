<div class="banner">
    <h2>
        <a href="index.html">Home</a>
        <i class="fa fa-angle-right"></i>
        <span>Dashboard</span>
    </h2>
</div>
		<div class="">
			<div class="agile-grids">	
			
				<!-- //Color-variations -->
				<div class="agile-buttons-grids">
					<!-- button-states -->
					<div class="col-sm-9 button-states-top-grid">
						<div class="panel button-states-grid">
							<div class="panel-heading">
								<div class="panel-title pn">
									<h3 class="mtn mb10 fw400">Inventory AS OF TODAY</h3>
								</div>
							</div>
								<?php 

								function getCount($value){
									 $count = mysql_fetch_array(mysql_query("SELECT count(*) FROM tbl_patient WHERE status = '$value'"));
									return $count[0];
								}
							$date = dateToday();

									echo '<table style="width: 100%;" class="table table-bordered" id="">';
										echo '<thead>';
									        echo '<tr>';
											    echo '<th>#</th>';
									            echo '<th>Product</th>';
									            echo '<th>Quantity</th>';
									        echo '</tr>';
									    echo '</thead>';
									    echo '<tbody>';
									        $query = mysql_query("SELECT * FROM tbl_product as p ORDER BY p.product_name ASC");
									        $count = 1;
									        while($row = mysql_fetch_array($query)){
									            $pID = $row['product_id'];
									            $quantity = mysql_fetch_array(mysql_query("SELECT sum(quantity) as tQntty FROM tbl_supply as s WHERE s.product_id = '$pID' AND s.date_added <= '$date'"));
									            $med_applied = mysql_fetch_array(mysql_query("SELECT sum(dosage) FROM tbl_med_vacc_sched WHERE product_id = '$pID' and status='1' AND sched_date <= '$date'"));
									            $diff= $quantity[0]-$med_applied[0];
									            $tQntty = ($diff == '')?0:$diff;
									            $color = ($diff <= 0)?'background-color: #fb9a9a !important;':'';
									        echo '<tr>';
									            echo '<td style="padding: 5px; font-size: 16px;'.$color.'">'.$count++.'</td>';
									            echo '<td style="padding: 5px; font-size: 16px;'.$color.'">'.$row["product_name"].'</td>';
									            echo '<td style="padding: 5px; font-size: 16px;'.$color.'">'.number_format($tQntty, 2).'</td>';
									        echo '</tr>';
									        }
									    echo '</tbody>';
									echo '</table>';
								?>
						</div>
					</div>
					<!-- //button-states -->
					
					<!-- icon-hover-effects -->
					<div class="col-sm-3 hover-buttons">
						<div class="wrap">
							<div class="bg-effect">
								<h3>Patient Status</h3>
								<div class="col-md-12">
									<div class="bg-danger light pv20 text-white fw600 text-center"><?php echo getCount('Admitted');?> Admitted <span class='fa fa-wheelchair '></span> </div>
									<div class="bg-system pv20 text-white fw600 text-center"> <?php echo getCount('Released');?> Released <span class='fa fa-thumbs-up'></span> </div>
									<div class="bg-primary light pv20 text-white fw600 text-center"> <?php echo getCount('Extended');?> Extended <span class='fa fa-expand'></span></div>
								</div>
							</div>
						</div>
					</div>
					<!-- //icon-hover-effects -->
					<div class="clearfix"> </div>
				</div>
				<!-- //buttons -->
			</div>
		</div>
		