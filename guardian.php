<style type="text/css">
	.wizard {
    margin: 20px auto;
    background: #fff;
}

    .wizard .nav-tabs {
        position: relative;
        margin-bottom: 0;
    }

    .wizard > div.wizard-inner {
        position: relative;	
    }

.connecting-line {
    height: 2px;
    background: #e0e0e0;
    position: absolute;
    width: 80%;
    margin: 0 auto;
    left: 0;
    right: 0;
    top: 50%;
    z-index: 1;
}

.wizard .nav-tabs > li.active > a, .wizard .nav-tabs > li.active > a:hover, .wizard .nav-tabs > li.active > a:focus {
    color: #555555;
    cursor: default;
    border-bottom-color: transparent;
}

span.round-tab {
    width: 70px;
    height: 70px;
    line-height: 70px;
    display: inline-block;
    border-radius: 100px;
    background: #fff;
    border: 2px solid #e0e0e0;
    z-index: 2;
    position: absolute;
    left: 0;
    text-align: center;
    font-size: 25px;
}
span.round-tab i{
    color:#555555;
}
.wizard li.active span.round-tab {
    background: #fff;
    border: 2px solid #5bc0de;
    
}
.wizard li.active span.round-tab i{
    color: #5bc0de;
}

span.round-tab:hover {
    color: #333;
    border: 2px solid #333;
}

.wizard .nav-tabs > li {
    width: 25%;
}

.wizard li:after {
    content: " ";
    position: absolute;
    left: 46%;
    opacity: 0;
    margin: 0 auto;
    bottom: 0px;
    border: 5px solid transparent;
    border-bottom-color: #5bc0de;
    transition: 0.1s ease-in-out;
}

.wizard li.active:after {
    content: " ";
    position: absolute;
    left: 46%;
    opacity: 1;
    margin: 0 auto;
    bottom: 0px;
    border: 10px solid transparent;
    border-bottom-color: #5bc0de;
}

.wizard .nav-tabs > li a {
    width: 70px;
    height: 70px;
    margin: 20px auto;
    border-radius: 100%;
    padding: 0;
}

    .wizard .nav-tabs > li a:hover {
        background: transparent;
    }

.wizard .tab-pane {
    position: relative;
}

.wizard h3 {
    margin-top: 0;
}

@media( max-width : 585px ) {

    .wizard {
        width: 90%;
        height: auto !important;
    }

    span.round-tab {
        font-size: 16px;
        width: 50px;
        height: 50px;
        line-height: 50px;
    }

    .wizard .nav-tabs > li a {
        width: 50px;
        height: 50px;
        line-height: 50px;
    }

    .wizard li.active:after {
        content: " ";
        position: absolute;
        left: 35%;
    }
}
</style>
<div class="banner">
    <h2>
        <a href="index.html">Home</a>
        <i class="fa fa-angle-right"></i>
        <span>Guardian</span>
    </h2>
</div>

<div class="blank">
    <div class="blank-page">
    <div class="table-heading">
		<h2>Guardian List</h2>
	</div>
	<div class="agile-tables">
		<div class="w3l-table-info">
			<div class="form-group pull-right">
				<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal" ><span class="fa fa-plus-circle"> </span> Add Guardian</button>
			</div>
			<table id="table">
			<thead>
				<tr>
				<th>#</th>
				<th>Name</th>
				<th>Address</th>
                <th>Email</th>
                <th>Contact No.</th>
				<th>Patient</th>
				<th>Action</th>


				</tr>
			</thead>
			<tbody>
			</tbody>
			</table>
			<?php require "modal/add_guardian.php";?>
			<?php require "modal/edit_guardian.php";?>
		</div>
	</div>
    </div>
</div>
<script src="js/jquery2.0.3.min.js"></script>
<script>

function editDetails(id){
    $("#editModal").modal("show");
    var parameter = "tbl_user where user_id ="+id;
    $.ajax({
        url:"ajax/getDetails.php",
        type:"POST",
        data:{
            parameter:parameter
        },success:function(data){
        var o = JSON.parse(data);

            
          $("#view_name").val(o.name);
          $("#view_address").val(o.address);
          $("#view_email").val(o.email);
          $("#datepicker2").val(o.dob);
          $("#contact_number").val(o.contact_number);

          $("#hidden_id").val(id)

        }
    });
}
function getData(){
  var table = $('#table').DataTable();
  table.destroy();
  $("#table").dataTable({
    "processing":true,
    "ajax":{
      "url":"ajax/datatables/dt_guardian.php",
      "dataSrc":"data"
    },
    "columns":[
       {
        "data":"count"
      },
      {
        "data":"name"
      },
      {
        "data":"address"
      },
      {
        "data":"email"
      },
      {
        "data":"contact"
      },
      {
        "data":"patient"
      },
      {
        "mRender": function(data,type,row){
           return "<center><button class='btn btn-success btn-sm' data-toggle='tooltip' title='Edit Record' value='"+ row.id+ "' id='"+row.id+"' onclick='editDetails("+row.id+")'><span class='fa fa-pencil'></span> Edit </button></center>";
          
        }
      }
    ]
  });
}


$(document).ready(function () {
      getData();
      $("#form_add").submit(function(e){
        e.preventDefault();

        $.ajax({
            type:"POST",
            url:"ajax/add_guardian.php",
            data:$(this).serialize(),
            success:function(data){
                if(data == 1){
                    notify('white','Successfully Added.');
                    getData();
                     $("#myModal").modal('hide');
                     $("#form_add")[0].reset();
                }else if(data == 2){
                    notify('white','Successfully Added.');
                }else{
                     notify('error','Duplicate Entry');
                }
            }
        });
      });
      $("#form_edit").submit(function(e){
        e.preventDefault(); 
        $.ajax({
            type:"POST",
            url:"ajax/update_guardian.php",
            data:$(this).serialize(),
            success:function(data){
             $("#editModal").modal('hide');
                if(data == 1){
                    notify('white','Successfully Added.');
                    getData();
                     $("#form_edit")[0].reset();
                }else if(data == 2){
                    notify('white','Successfully Updated.');
                }else{
                    notify('warning','Error. Please try again later.');
                }
            }
        });
      });


    //Initialize tooltips
    $('.nav-tabs > li a[title]').tooltip();
    
    //Wizard
    $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {

        var $target = $(e.target);
    
        if ($target.parent().hasClass('disabled')) {
            return false;
        }
    });

    $(".next-step").click(function (e) {

        var $active = $('.wizard .nav-tabs li.active');
        $active.next().removeClass('disabled');
        nextTab($active);

    });
    $(".prev-step").click(function (e) {

        var $active = $('.wizard .nav-tabs li.active');
        prevTab($active);

    });
});

function nextTab(elem) {
    $(elem).next().find('a[data-toggle="tab"]').click();
}
function prevTab(elem) {
    $(elem).prev().find('a[data-toggle="tab"]').click();
}
</script>