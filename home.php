<?php
include 'function.php';
$page = (isset($_GET['page']) && $_GET['page'] !='') ? $_GET['page'] : '';
$view = (isset($_GET['view']) && $_GET['view'] !='') ? $_GET['view'] : '';
$sub = (isset($_GET['sub']) && $_GET['sub'] !='') ? $_GET['sub'] : '';

 session_start();
  $id= $_SESSION['id'];
  $name= $_SESSION['name'];
  $status= $_SESSION['status'];
  if($status == ""){
    header("Location:ajax/logout.php");
  }else{
    $in = "in";
  }
$dateToday = dateToday();

  $fetch_sched = mysql_query("SELECT * from tbl_med_vacc_sched where status = 0 and sched_date='$dateToday'");
  $count_sched = mysql_num_rows($fetch_sched);

  $finish_sched = mysql_query("SELECT * from tbl_med_vacc_sched where status = 1 and sched_date='$dateToday'");
  $count_finish_sched = mysql_num_rows($finish_sched);



  $count_all = $count_finish_sched+$count_sched;

  if($count_all == 0){
  $perc ="0%";

  }else{
  $perc = (($count_finish_sched/$count_all)*100)."%";

  }

?>
<!DOCTYPE html>
<head>
<title>Treatment and Rehabilition</title>
<meta name="viewport" content="width=device-width, initial-scale=1">

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>

 <link rel="icon" type="image/ico" href="images/logo.png" />
<link rel="stylesheet" href="css/bootstrap.css">
<link rel='stylesheet' type='text/css' href="css/style.css" />
<link rel="stylesheet" href="css/font.css" type="text/css"/>
<link rel="stylesheet" href="css/font-awesome.css"> 
<link rel="stylesheet" type="text/css" href="css/table-style.css" />
<link rel="stylesheet" type="text/css" href="css/basictable.css" />
<link rel="stylesheet" type="text/css" href="css/jquery.dataTables.min.css" />

<!--// Datepicker------>
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css" />
<link rel="stylesheet" type="text/css" href="css/datepicker.css"/>
<!-- //font-awesome icons -->	

<!--// Multiselect------>
<link rel="stylesheet" type="text/css" href="css/bootstrap-multiselect.css" />

	<link href="dist/styles/metro/notify-metro.css" rel="stylesheet" />


<style>

.dropdown-menu{
		max-height: 300px;
    	overflow-y: auto;
	}
	.multiselect-group{
		background-color: #d8d5d5;
	}

	.notifyjs-metro-base .image{
		width: 25%;
		left: 65px !important;
	}
</style>
</head>
<body class="dashboard-page">

	<nav class="main-menu">
		<ul>
			<li>
				<a href="home.php?page=dashboard">
					<i class="fa fa-home nav_icon"></i>
					<span class="nav-text">
					Dashboard
					</span>
				</a>
            </li>
            <li>
				<a href="home.php?page=guardian">
					<i class="fa fa-users nav_icon"></i>
					<span class="nav-text">
					Guardian
					</span>
				</a>
            </li>
            <li>
				<a href="home.php?page=medvacc">
					<i class="fa fa-calendar nav-icon"></i>
					<span class="nav-text">
					Med and Vac Schedule
					</span>
				</a>
            </li>
            <li>
				<a href="home.php?page=product">
					<i class="fa fa-medkit nav_icon"></i>
					<span class="nav-text">
					Product
					</span>
				</a>
            </li>
            
            <li>
				<a href="home.php?page=patient">
					<i class="fa fa-wheelchair nav_icon"></i>
					<span class="nav-text">
					Patient
					</span>
				</a>
			</li>
			<li class="has-subnav">
				<a href="javascript:;">
					<i class="fa fa-file-text-o nav_icon"></i>
						<span class="nav-text">Reports</span>
					<i class="icon-angle-right"></i><i class="icon-angle-down"></i>
				</a>
				<ul>
					<li>
						<a class="subnav-text" href="home.php?page=inv">
							Inventory Report
						</a>
					</li>
					<li>
						<a class="subnav-text" href="home.php?page=medvaccReport">
							Med and Vac Report
						</a>
					</li>
					<!-- <li>
						<a class="subnav-text" href="home.php?page=missedSchedReport">
							Missed Schedule Report
						</a>
					</li> -->
				</ul>
            </li>
            <li>
				<a href="home.php?page=supply">
					<i class="fa fa-truck nav_icon"></i>
					<span class="nav-text">
					Supply
					</span>
				</a>
            </li>
			
          <!--   <li>
				<a href="javascript:;">
				<i class="fa fa-check-square-o nav_icon"></i>
				<span class="nav-text">
				Task
				</span>
				
            </li> -->
		</ul>
		<ul class="logout">
			<li>
			<a href="login.html">
			<i class="icon-off nav-icon"></i>
			<span class="nav-text">
			Logout
			</span>
			</a>
			</li>
		</ul>
	</nav>
	<section class="wrapper scrollable">
		<nav class="user-menu">
			<a href="javascript:;" class="main-menu-access">
			<i class="icon-proton-logo"></i>
			<i class="icon-reorder"></i>
			</a>
		</nav>
		<section class="title-bar">
			<div class="logo" style="width: 60%;">
				<h1><a href="#" style="text-decoration: none;"><img src="images/logo.png" alt="" style="    width: 8%;" />Treatment and Rehabilition</a></h1>
			</div>
			
			<div class="header-right">
				<div class="profile_details_left">
					<div class="header-right-left">
						<!--notifications of menu start -->
						<ul class="nofitications-dropdown">
							<li class="dropdown head-dpdn">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-bell"></i><span class="badge blue"><?php echo $count_finish_sched;?></span></a>
								<ul class="dropdown-menu anti-dropdown-menu agile-notification">
									<li>
										<div class="notification_header">
											<h3>You have <?php echo $count_finish_sched;?> new notifications</h3>
										</div>
									</li>
									<?php 
									while($row_finish = mysql_fetch_array($finish_sched)){
									echo '<li><a href="#">';
										echo '<div class="user_img"><img src="images/check.png" alt=""></div>';
									   echo '<div class="notification_desc">';
										echo '<p>'.getPatient($row_finish['patient_id']).'</p>';
										echo '<p><span>'.date("M d, Y h:i a",strtotime($row_finish['date_applied'])).'</span></p>';
										echo '</div>';
									   echo '<div class="clearfix"></div>';
									 echo '</a></li>';
									}?>
									 
									 <li>
										<div class="notification_bottom">
											<a href="home.php?page=finish">See all notifications</a>
										</div> 
									</li>
								</ul>
							</li>	
							<li class="dropdown head-dpdn">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-tasks"></i><span class="badge blue1"><?php echo $count_sched;?></span></a>
								<ul class="dropdown-menu anti-dropdown-menu agile-task">
									<li>
										<div class="notification_header">
											<h3>Staff have <?php echo $count_sched;?> pending tasks</h3>
										</div>
									</li>
									<li><a href="#">
										<div class="task-info">
											<span class="task-desc">MedVacc update</span><span class="percentage"><?php echo $perc;?></span>
											<div class="clearfix"></div>	
										</div>
										<div class="progress progress-striped active">
											<div class="bar yellow" style="width:<?php echo $perc;?>"></div>
										</div>
									</a></li>
									<li>
										<div class="notification_bottom">
											<a href="home.php?page=pending">See all pending tasks</a>
										</div> 
									</li>
								</ul>
							</li>	
							<div class="clearfix"> </div>
						</ul>
					</div>	
					<div class="profile_details">		
						<ul>
							<li class="dropdown profile_details_drop">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
									<div class="profile_img">	
										<span class="prfil-img"><i class="fa fa-user" aria-hidden="true"></i></span> 
										<div class="clearfix"></div>	
									</div>	
								</a>
								<ul class="dropdown-menu drp-mnu" style="left:-160px !important;">
									<li> <a href="home.php?page=staff"><i class="fa fa-user-md"></i> Staff</a> </li> 
									<li> <a href="home.php?page=profile"><i class="fa fa-cog"></i> Profile</a> </li> 
									<li> <a href="home.php?page=ch_pw"><i class="fa fa-key"></i> Change Password</a> </li> 
									<li> <a href="ajax/logout.php"><i class="fa fa-sign-out"></i> Logout</a> </li>
								</ul>
							</li>
						</ul>
					</div>
					<div class="clearfix"> </div>
				</div>
			</div>
			<div class="clearfix"> </div>
		</section>
		<div class="main-grid">
			<div class="agile-grids">	
				<!-- blank-page -->
				<?php
				if($page == 'dashboard'){
					require 'dashboard.php';
				}else if($page == 'product'){
					require 'product.php';
				}else if($page == 'supply'){
					require 'supply.php';	
				}else if($page == 'guardian'){
					require 'guardian.php';
				}else if($page == 'patient'){
					require 'patient.php';
				}else if($page == 'medvacc'){
					require 'medvacc.php';
				}else if($page == 'inv'){
					require 'inv.php';
				}else if($page == 'medvaccReport'){
					require 'medvaccReport.php';
				}else if($page == 'missedSchedReport'){
					require 'missedSchedReport.php';
				}else if($page == 'staff'){
					require 'staff.php';
				}else if($page == 'profile'){
					require 'profile.php';
				}else if($page == 'ch_pw'){
					require 'ch_pw.php';
				}else if($page == 'pending'){
					require 'pending.php';
				}else if($page == 'finish'){
					require 'finish.php';
				}else{	
					require 'views/404.php';
				}?>
				<!-- //blank-page -->
				
			</div>
		</div>
		
		<!-- footer -->
		<div class="footer">
			<p>© <?php echo date("Y");?> Treatment and Rehabilitaion . All Rights Reserved . </a></p>
		</div>
		<!-- //footer -->
	</section>
	<script src="js/jquery2.0.3.min.js"></script>
	<script src="js/modernizr.js"></script>
	<script src="js/jquery.cookie.js"></script>
	<script src="js/screenfull.js"></script>
	<!-- <script src="js/bootstrap.js"></script> -->
	
	<script src="js/proton.js"></script>
	<script type="text/javascript" src="js/jquery.basictable.min.js"></script>
	<script type="text/javascript" src="js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="js/bootstrap.min.js"></script>
	<script src="js/bootstrap-datepicker.js"></script>
	<script  src="js/script.js"></script>
	<script type="text/javascript" src="js/bootstrap-multiselect.js"></script>
	<script type="text/javascript" src="js/date-time-picker.min.js"></script>
	<script src="dist/notify.js"></script>
	<script src="dist/styles/metro/notify-metro.js"></script>

	<script type="text/javascript">
	$(function () {
				$('#supported').text('Supported/allowed: ' + !!screenfull.enabled);
				if (!screenfull.enabled) {
					return false;
				}
				$('#toggle').click(function () {
					screenfull.toggle($('#container')[0]);
				});	
			});
		$(document).ready(function() {

		$('#table').DataTable();
		
		$('#select_search_all').multiselect({
            includeSelectAllOption: true,
            enableFiltering: true,
            enableCaseInsensitiveFiltering: true,
            filterPlaceholder: 'Search Patient'
        }); 

		$("#datepicker1").dateTimePicker();
		$("#datepicker").dateTimePicker();
		$("#datepicker2").dateTimePicker();
		$("#sched_date").dateTimePicker();
		$("#sched_date1	").dateTimePicker();

		});

		function notify(color,msg) {
        $.notify({
            title: 'Alert Notification',
            text: msg,
            image: "<img src='images/check.png'/ style='width: 35%;'>"
        }, {
            style: 'metro',
            className: color,
            autoHide: false,
            clickToHide: true
        });

        // setTimeout(function(){
        // 	 $(".notifyjs-wrapper").fadeOut();
        // },2000)
    }
	</script>
</body>
</html>