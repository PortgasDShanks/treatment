<style>
	button.btn.btn-default{
		width: 100%;
		background: white;
		border: 1px solid #c1c1c1;
		color: #c1c1c1;
		padding: 5px;
		text-align: left;
	}

	.btn-group, .btn-group-vertical{
		width: 100%;
	}
</style>
<div class="banner">
    <h2>
        <a href="index.html">Home</a>
        <i class="fa fa-angle-right"></i>
        <span>Medication and Vaccination Schedule</span>
    </h2>
</div>

<div class="blank">
    <div class="blank-page">
    <div class="table-heading">
		<h2>Medication and Vaccination Schedule List</h2>
	</div>
	<div class="agile-tables">
		<div class="w3l-table-info">
			<div class="form-group pull-right">
				<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal" ><span class="fa fa-plus-circle"> </span> Add Schedule</button>
			</div>
			<table id="table">
			<thead>
				<tr>
				<th>#</th>
				<th>Patient Name</th>
				<th>Product</th>
				<th>Dosage</th>
				<th>Schedule</th>
        <th>Assigned Nurse</th>
				<th>Remarks</th>
				<th>Action</th>

				</tr>
			</thead>
			<tbody>
				
			</tbody>
			</table>
			<?php require "modal/add_medvacc.php";?>
      <?php require "modal/modal_response_log.php";?>
			<?php require "modal/edit_medvacc.php";?>

		</div>
	</div>
    </div>
</div>

<script src="js/jquery2.0.3.min.js"></script>
<script>
$(document).ready(function(){
  	getData();

    $("#form_add").submit(function(e){
  e.preventDefault();
  var select_product_id = $("#select_product_id").val();
  var select_nurse = $("#select_nurse").val();

  $.ajax({
    url:"ajax/add_medvacc_sched.php",
    method:"POST",
    data:$(this).serialize(),
    success:function(data){
    $("#myModal").modal('hide');
    $("#modalResponse").modal("show");
    $("#modal_body").html(data);
      getData();
      $("#form_add")[0].reset();
    }
  })
   });

$("#form_edit").submit(function(e){
  e.preventDefault();
  $.ajax({
    url:"ajax/update_medvac.php",
    method:"POST",
    data:$(this).serialize(),
    success:function(data){
    $("#editModal").modal('hide');
    if(data==1){
       notify('white','Successfully Updated');
      getData();
     
      $("#form_edit")[0].reset();
    }else if(data==2){
       notify('warning','Duplicate Entry');

    }else{
       notify('error','Duplicate Entry');

    }
    }
  })
   });
});
function editDetails(id){
  $("#editModal").modal("show");
  var parameter = "tbl_med_vacc_sched where med_vacc_sched_id ="+id;

    $.ajax({
        url:"ajax/getDetails.php",
        type:"POST",
        data:{
            parameter:parameter
        },success:function(data){
          var o = JSON.parse(data);


          $("#product_id").val(o.product_id);
          $("#dosage").val(o.dosage);
          $("#sched_date1").val(o.sched_date);
          $("#remarks").val(o.remarks);
          $("#assign_nurse_id").val(o.assign_nurse_id)
          $("#hidden_id").val(id);

        }
    });
}
function getData(){
  var table = $('#table').DataTable();
  table.destroy();
  $("#table").dataTable({
    "processing":true,
    "ajax":{
      "url":"ajax/datatables/dt_medvacc.php",
      "dataSrc":"data"
    },
    "columns":[
       {
        "data":"count"
      },
      {
        "data":"patient_name"
      },
      {
        "data":"product"
      },
      {
        "data":"dosage"
      },
      {
        "data":"sched_date"
      },
      {
        "data":"nurse"
      },
      {
        "data":"remarks"
      },
      {
        "mRender": function(data,type,row){
           return "<center><button class='btn btn-success btn-sm' data-toggle='tooltip' title='Edit Record' value='"+ row.id+ "' id='"+row.id+"' onclick='editDetails("+row.id+")'><span class='fa fa-pencil'></span> Edit </button></center>";
          
        }
      }
    ]
  });
}
</script>