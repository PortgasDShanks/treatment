<div class="banner">
    <h2>
        <a href="home.php?page=dashboard">Home</a>
        <i class="fa fa-angle-right"></i>
        <span>Report</span>
        <i class="fa fa-angle-right"></i>
        <span>Missed Schedule Report</span>

    </h2>
</div>

<div class="blank">
    <div class="blank-page">
    <div class="table-heading">
		<h2>Missed Schedule Report</h2>
	</div>
	<div class="agile-tables">
		<div class="w3l-table-info">
			<div class="form-group col-sm-12"> 
				<div class='col-md-4'>
					<div class='input-group'>
						<span class='input-group-addon'><strong>Start Date :</strong></span>
						<input type='text' class='form-control' id='datepicker' >
					</div>
				</div>
				
				<div class='col-md-4'>
					<div class='input-group'>
						<span class='input-group-addon'><strong>End Date :</strong></span>
						<input type='text' class='form-control' id='datepicker1' >
					</div>
				</div>
				<div class='col-md-4'>
					<button type="button" class="btn btn-primary"><span class='fa fa-refresh'></span> Generate </button>
				</div>
            </div>

       
           
		</div>
		<div class="row" id="report_data">
           </div>
	</div>
    </div>
</div>