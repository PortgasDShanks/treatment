<form class="form-horizontal" id="form_add" method="post"> 
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Add Schedule</h4>
      </div>
      <div class="modal-body">
      <div class="form-body">
           <div class="form-group"> 
            <label for="inputEmail3" class="col-sm-2 control-label">Product</label> 
            <div class="col-sm-9"> 
            <select name="product_id" id="select_product_id" class="form-control1"  required="">
               <option value="">--Please select Product--</option>
                <?php 
                include "core/config.php";

                $fetch_product =mysql_query("SELECT * FROM tbl_product");
                while($row=mysql_fetch_array($fetch_product)){
                  echo "<option value=".$row['product_id'].">".$row['product_name']."</option>";
                }
                ?>
               </select>
            </div> 
          </div> 
          <div class="form-group"> 
            <label for="inputEmail3" class="col-sm-2 control-label">Patient Name</label> 
            <div class="col-sm-9"> 
            <select name="patient_id[]" id="select_search_all" multiple  class="form-control1" required="">
                <?php 
                include "core/config.php";

                $fetch_product =mysql_query("SELECT * FROM tbl_patient");
                while($row=mysql_fetch_array($fetch_product)){
                  echo "<option value=".$row['patient_id'].">".$row['patient_name']."</option>";
                }
                ?>
            </select>
            </div> 
          </div> 
           <div class="form-group"> 
              <label for="inputEmail3" class="col-sm-2 control-label">Dosage:</label> 
              <div class="col-sm-9"> 
                <input type="number" min="0.1" step="0.1" name="dosage" class="form-control" required placeholder="Dosage"> 
              </div> 
            </div> 
            <div class="form-group"> 
              <label for="inputPassword3" class="col-sm-2 control-label">Schedule:</label> 
              <div class="col-sm-9"> 
                <input type="text" name="sched_date" id="sched_date" class="form-control" required placeholder="Schedule Date"> 
              </div> 
            </div>
             <div class="form-group"> 
              <label for="inputPassword3" class="col-sm-2 control-label">Remarks:</label> 
              <div class="col-sm-9"> 
                <input type="text" name="remarks" class="form-control" placeholder="Remarks"> 
              </div> 
            </div>
             <div class="form-group"> 
            <label for="inputEmail3" class="col-sm-2 control-label">Assign Nurse</label> 
            <div class="col-sm-9"> 
            <select name="assign_nurse_id" id="select_nurse" class="form-control1" required="">
               <option value="">--Please select Nurse--</option>
                <?php 
                include "core/config.php";

                $fetch_product =mysql_query("SELECT * FROM tbl_user where status = 'N'");
                while($row=mysql_fetch_array($fetch_product)){
                  echo "<option value=".$row['user_id'].">".$row['name']."</option>";
                }
                ?>
               </select>
            </div> 
          </div> 
      </div>
      </div>
      <div class="modal-footer">
      <button type="submit" class="btn btn-success" >Save</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
</form> 