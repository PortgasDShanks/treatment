<form class="form-horizontal" id="form_add" method="post"> 
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Add Supply</h4>
      </div>
      <div class="modal-body">
      <div class="form-body">
          <div class="form-group"> 
            <label for="inputEmail3" class="col-sm-2 control-label">Product</label> 
            <div class="col-sm-9"> 
            <select name="product_id" id="selector1" class="form-control1" required="">
                <option>--Please select Product--</option>
                <?php 
                include "core/config.php";

                $fetch_product =mysql_query("SELECT * FROM tbl_product");
                while($row=mysql_fetch_array($fetch_product)){
                  echo "<option value=".$row['product_id'].">".$row['product_name']."</option>";
                }
                ?>
            </select>
            </div> 
          </div> 
          <div class="form-group"> 
            <label for="inputPassword3" class="col-sm-2 control-label">Quantity</label> 
            <div class="col-sm-9"> 
              <input type="number" name="quantity" min="0" step="1" class="form-control" required placeholder="Quantity" autocomplete="off"> 
            </div> 
          </div>
          <div class="form-group"> 
            <label for="inputPassword3" class="col-sm-2 control-label">Date Added</label> 
            <div class="col-sm-9"> 
              <input type="text" name="date_added" class="form-control" id="datepicker1" required placeholder="Date Added" autocomplete="off"> 
            </div> 
          </div>
      </div>
      </div>
      <div class="modal-footer">
      <button type="submit" class="btn btn-success" >Save</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
</form> 