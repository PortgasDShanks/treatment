<form class="form-horizontal" id="form_edit" method="post"> 
<div id="editModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Edit Schedule</h4>
      </div>
      <div class="modal-body">
      <div class="form-body">
        <input type="hidden" name="med_vacc_sched_id" class="form-control" id="hidden_id" required placeholder="Product"> 
         <div class="form-group"> 
            <label for="inputEmail3" class="col-sm-2 control-label">Product</label> 
            <div class="col-sm-9"> 
            <select name="product_id" id="product_id" class="form-control1">
                <option>--Please select Product--</option>
                <?php 
                include "core/config.php";

                $fetch_product =mysql_query("SELECT * FROM tbl_product");
                while($row=mysql_fetch_array($fetch_product)){
                  echo "<option value=".$row['product_id'].">".$row['product_name']."</option>";
                }
                ?>
            </select>
            </div> 
          </div> 
           <div class="form-group"> 
              <label for="inputEmail3" class="col-sm-2 control-label">Dosage:</label> 
              <div class="col-sm-9"> 
                <input type="number" name="dosage" min="0.1" step="0.1" name="dosage" id="dosage"class="form-control" required placeholder="Dosage" autocomplete="off"> 
              </div> 
            </div> 
            <div class="form-group"> 
              <label for="inputPassword3" class="col-sm-2 control-label">Schedule:</label> 
              <div class="col-sm-9"> 
                <input type="text" name="sched_date" id="sched_date1" class="form-control" required placeholder="Schedule Date" autocomplete="off"> 
              </div> 
            </div>
             <div class="form-group"> 
              <label for="inputPassword3" class="col-sm-2 control-label">Remarks:</label> 
              <div class="col-sm-9"> 
                <input type="text" name="remarks" id="remarks" class="form-control" required placeholder="Remarks" autocomplete="off">  
              </div> 
            </div>

            <div class="form-group"> 
            <label for="inputEmail3" class="col-sm-2 control-label">Assign Nurse</label> 
            <div class="col-sm-9"> 
            <select name="assign_nurse_id" id="assign_nurse_id" class="form-control1">
               <option>--Please select Nurse--</option>
                <?php 
                include "core/config.php";

                $fetch_product =mysql_query("SELECT * FROM tbl_user where status = 'N'");
                while($row=mysql_fetch_array($fetch_product)){
                  echo "<option value=".$row['user_id'].">".$row['name']."</option>";
                }
                ?>
               </select>
            </div> 
          </div> 

      </div>
      </div>
      <div class="modal-footer">
      <button type="submit" class="btn btn-success">Update</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
</form> 