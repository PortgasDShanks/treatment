<form class="form-horizontal" id="form_edit" method="post"> 
<div id="editModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Edit Staff</h4>
      </div>
      <div class="modal-body">
      <div class="form-body">
         <input type="hidden" name="user_id" class="form-control" id="hidden_id" required placeholder="Staff"> 
           <div class="form-group"> 
            <label for="inputEmail3" class="col-sm-2 control-label">Name:</label> 
            <div class="col-sm-9"> 
              <input type="text" name="name" class="form-control" id="name" required placeholder="Name" autocomplete="off"> 
            </div> 
          </div> 
          <div class="form-group"> 
            <label for="inputPassword3" class="col-sm-2 control-label">Address:</label> 
            <div class="col-sm-9"> 
              <input type="text" name="address" class="form-control" id="address" required placeholder="Address" autocomplete="off" > 
            </div> 
          </div>
          <div class="form-group"> 
            <label for="inputPassword3" class="col-sm-2 control-label">Birthdate:</label> 
            <div class="col-sm-9"> 
              <input type="text" name="dob" id="datepicker2" class="form-control" required placeholder="Date of Birth" autocomplete="off"> 
            </div> 
          </div>
           <div class="form-group"> 
            <label for="inputPassword3" class="col-sm-2 control-label">Email:</label> 
            <div class="col-sm-9"> 
              <input type="email" name="email" id="email" class="form-control" required placeholder="Email" autocomplete="off">
            </div> 
        </div>
      </div>
    </div>
      <div class="modal-footer">
      <button type="submit" class="btn btn-success" >Update</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
</form> 