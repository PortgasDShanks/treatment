<div id="modalResponse" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Response Log</h4>
      </div>
      <div class="modal-body" id="modal_body">
     
      </div>
    </div>
  </div>
</div>