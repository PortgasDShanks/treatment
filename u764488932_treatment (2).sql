-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Oct 03, 2020 at 09:54 AM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.6.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `u764488932_treatment`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_med_vacc_sched`
--

CREATE TABLE `tbl_med_vacc_sched` (
  `med_vacc_sched_id` int(11) NOT NULL,
  `patient_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `dosage` decimal(12,3) NOT NULL,
  `sched_date` date NOT NULL,
  `remarks` varchar(225) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `read_status` int(1) NOT NULL,
  `date_applied` datetime NOT NULL,
  `assign_nurse_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_med_vacc_sched`
--

INSERT INTO `tbl_med_vacc_sched` (`med_vacc_sched_id`, `patient_id`, `product_id`, `dosage`, `sched_date`, `remarks`, `status`, `read_status`, `date_applied`, `assign_nurse_id`) VALUES
(1, 1, 1, '1.600', '2020-04-26', 'dsadas', 0, 0, '2020-04-26 21:05:52', 2),
(3, 1, 1, '1.600', '2020-04-24', 'dsadas', 1, 1, '2020-04-24 08:00:00', 2),
(4, 1, 1, '1.000', '2020-04-23', '1', 1, 1, '2020-04-23 08:00:00', 2),
(5, 2, 1, '1.000', '2020-04-23', '1', 0, 0, '0000-00-00 00:00:00', 2),
(6, 3, 1, '1.000', '2020-04-23', '1', 0, 0, '0000-00-00 00:00:00', 2),
(7, 1, 1, '1.000', '2020-04-23', '', 0, 0, '0000-00-00 00:00:00', 2),
(8, 1, 1, '2.000', '2020-04-23', '', 0, 0, '0000-00-00 00:00:00', 6),
(9, 2, 1, '2.000', '2020-04-23', '', 0, 0, '0000-00-00 00:00:00', 6),
(10, 3, 1, '2.000', '2020-04-23', '', 0, 0, '0000-00-00 00:00:00', 6),
(11, 1, 1, '1.000', '2020-04-23', '', 0, 0, '0000-00-00 00:00:00', 2),
(12, 2, 1, '1.000', '2020-04-23', '', 0, 0, '0000-00-00 00:00:00', 2),
(13, 3, 1, '1.000', '2020-04-23', '', 0, 0, '0000-00-00 00:00:00', 2);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_patient`
--

CREATE TABLE `tbl_patient` (
  `patient_id` int(11) NOT NULL,
  `guardian_id` int(11) NOT NULL,
  `patient_code` varchar(225) COLLATE utf8mb4_unicode_ci NOT NULL,
  `patient_name` varchar(225) COLLATE utf8mb4_unicode_ci NOT NULL,
  `patient_dob` date NOT NULL,
  `patient_address` varchar(225) COLLATE utf8mb4_unicode_ci NOT NULL,
  `patient_remarks` varchar(225) COLLATE utf8mb4_unicode_ci NOT NULL,
  `patient_assign_nurse_id` int(11) NOT NULL,
  `patient_added_user_id` int(11) NOT NULL,
  `status` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_patient`
--

INSERT INTO `tbl_patient` (`patient_id`, `guardian_id`, `patient_code`, `patient_name`, `patient_dob`, `patient_address`, `patient_remarks`, `patient_assign_nurse_id`, `patient_added_user_id`, `status`) VALUES
(1, 3, 'bje072396', 'kim shin yo', '1990-03-01', 'dsadas sadasd', 'psycopath', 2, 3, 'Released'),
(2, 3, 'abs012395', 'Park sae royi', '1990-03-01', 'brgy.tangub', 'lain ulo', 2, 3, 'Released'),
(3, 4, 'kdd20200420', 'kim dshin dyo', '2020-04-20', 'fsdf', 'gfdgf', 0, 0, 'Admitted');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_product`
--

CREATE TABLE `tbl_product` (
  `product_id` int(11) NOT NULL,
  `product_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_desc` varchar(2255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_product`
--

INSERT INTO `tbl_product` (`product_id`, `product_name`, `product_desc`) VALUES
(1, 'sample', 'sadsa'),
(2, 'dsa', 'sadsa');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_supply`
--

CREATE TABLE `tbl_supply` (
  `supply_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `date_added` date NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_supply`
--

INSERT INTO `tbl_supply` (`supply_id`, `product_id`, `quantity`, `date_added`, `user_id`) VALUES
(1, 1, 12, '2020-04-23', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

CREATE TABLE `tbl_user` (
  `user_id` int(11) NOT NULL,
  `un` varchar(225) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pw` varchar(225) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_added` date NOT NULL,
  `name` varchar(225) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dob` date NOT NULL,
  `address` varchar(225) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact_number` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(225) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(1) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_user`
--

INSERT INTO `tbl_user` (`user_id`, `un`, `pw`, `date_added`, `name`, `dob`, `address`, `contact_number`, `email`, `status`) VALUES
(1, 'a', 'z', '2020-03-26', 'sample aaaaa', '1996-12-07', 'brgy.16', '', 'a@gmail.com', 'A'),
(2, 'nurse', '12345', '2020-03-29', 'nurse 1', '1980-03-01', 'dasas', '', 'faksh@gmail.com', 'N'),
(3, 'admin', '12345', '2020-03-01', 'John Doe', '2000-04-05', 'aaaa', '09107505919', 'aaa@gmail.com', 'G'),
(4, 'DD20200420', '12345', '2020-04-20', 'Christine Smith', '2020-04-20', 'DSA', '', 'SADSA@gmail.com', 'G'),
(5, 'ff20200420', '12345', '2020-04-20', 'Lyca wakay', '2020-04-20', 'sdfds', '09107505919', 'fdsf@sdfsd.com', 'G'),
(6, '', '', '2020-04-22', 'dsadsa', '2020-04-22', 'ds', '', 'dsad@gmail.com', 'N'),
(7, 'SO20000929', '12345', '2020-10-03', 'Sham Once ', '2000-09-29', 'taculing', '09107505919', 'test@gmail.com', 'G'),
(8, 'KS20000929', '12345', '2020-10-03', 'Karina Sy', '2000-09-29', 'taculing', '09107505919', 'test@gmail.com', 'G');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_med_vacc_sched`
--
ALTER TABLE `tbl_med_vacc_sched`
  ADD PRIMARY KEY (`med_vacc_sched_id`);

--
-- Indexes for table `tbl_patient`
--
ALTER TABLE `tbl_patient`
  ADD PRIMARY KEY (`patient_id`);

--
-- Indexes for table `tbl_product`
--
ALTER TABLE `tbl_product`
  ADD PRIMARY KEY (`product_id`);

--
-- Indexes for table `tbl_supply`
--
ALTER TABLE `tbl_supply`
  ADD PRIMARY KEY (`supply_id`);

--
-- Indexes for table `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_med_vacc_sched`
--
ALTER TABLE `tbl_med_vacc_sched`
  MODIFY `med_vacc_sched_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `tbl_patient`
--
ALTER TABLE `tbl_patient`
  MODIFY `patient_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tbl_product`
--
ALTER TABLE `tbl_product`
  MODIFY `product_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tbl_supply`
--
ALTER TABLE `tbl_supply`
  MODIFY `supply_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tbl_user`
--
ALTER TABLE `tbl_user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
